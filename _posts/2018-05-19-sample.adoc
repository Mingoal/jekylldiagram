---
layout: post
title:  "Asciidoctor Diagram + Jekyll Sample"
date:   2018-05-19 10:15:28 +0800
categories: jekyll update
---
= Asciidoctor Diagram + Jekyll Sample
:uri-asciidoctor: http://asciidoctor.org

This is a sample page composed in AsciiDoc.
Jekyll converts it to HTML using {uri-asciidoctor}[Asciidoctor].

[source,ruby]
puts "Hello, World!"

[plantuml, hello-jekyll, png]
....
Tom -> Billy : Hello
....